import requests
import pprint

pp = pprint.PrettyPrinter(indent=2)

data = requests.get('https://reddit.com/.json', headers = {'User-agent': 'dionis'})

for post in data.json()['data']['children']:
    print(post['data']['num_comments'])